<?php
/**
 * @file
 * Contains \Drupal\restui\Controller\RestUIController.
 */

namespace Drupal\restui\Controller;

use Drupal\Core\Controller\ControllerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\restui\RestUIManager;
use Drupal\rest\Plugin\Type\ResourcePluginManager;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Controller routines for REST resources.
 */
class RestUIController implements ControllerInterface {

  /**
   * Rest UI Manager Service.
   *
   * @var \Drupal\restui\RestUIManager
   */
  protected $restUIManager;

  /**
   * Resource plugin manager.
   *
   * @var \Drupal\rest\Plugin\Type\ResourcePluginManager
   */
  protected $resourcePluginManager;

  /**
   * The URL generator to use.
   *
   * @var \Symfony\Component\Routing\Generator\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * Injects RestUIManager Service.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('restui.manager'),
      $container->get('plugin.manager.rest'),
      $container->get('url_generator')
    );
  }

  /**
   * Constructs a RestUIController object.
   */
  public function __construct(RestUIManager $restUIManager, ResourcePluginManager $resourcePluginManager, UrlGeneratorInterface $url_generator) {
    $this->restUIManager = $restUIManager;
    $this->resourcePluginManager = $resourcePluginManager;
    $this->urlGenerator = $url_generator;
  }

  /**
   * Returns an administrative overview of all REST resources.
   *
   * @return string
   *   A HTML-formatted string with the administrative page content.
   *
   */
  public function listResources() {
    // Get the list of enabled resources.
    $config = config('rest.settings')->get('resources') ?: array();
    // Strip out the nested method configuration, we are only interested in the
    // plugin IDs of the resources.
    $enabled_resources = drupal_map_assoc(array_keys($config));

    $available_resources = array();
    $resources = $this->resourcePluginManager->getDefinitions();
    foreach ($resources as $id => $resource) {
      $plugin = $this->resourcePluginManager->getInstance(array('id' => $id));
      $status = in_array($id, $enabled_resources) ? 'enabled' : 'disabled';
      $available_resources[$status][$id] = $resource;
    }

    // Sort the list of resources by label.
    $sort_resources = function($resource_a, $resource_b) {
      return strcmp($resource_a['label'], $resource_b['label']);
    };
    uasort($available_resources['enabled'], $sort_resources);
    uasort($available_resources['disabled'], $sort_resources);

    $list['resources_title'] = array(
      '#markup' => '<h2>' . t('REST resources') . '</h2>',
    );
    $list['resources_help'] = array(
      '#markup' => '<p>' . t('Here you can enable and disable available resources. Once a resource ' .
                             'has been enabled, you can restrict its formats and authentication by ' .
                             'clicking on its "Edit" link.') . '</p>',
    );
    $list['enabled']['heading']['#markup'] = '<h2>' . t('Enabled') . '</h2>';
    $list['disabled']['heading']['#markup'] = '<h2>' . t('Disabled') . '</h2>';

    foreach (array('enabled', 'disabled') as $status) {
      $list[$status]['#type'] = 'container';
      $list[$status]['#attributes'] = array('class' => array('rest-ui-list-section', $status));
      $list[$status]['table'] = array(
        '#theme' => 'table',
        '#header' => array(
          'resource_name' => array(
            'data' => t('Resource name'),
            'class' => array('rest-ui-name'),
          ),
          'path' => array(
            'data' => t('Path'),
            'class' => array('views-ui-path'),
          ),
          'description' => array(
            'data' => t('Description'),
            'class' => array('rest-ui-description'),
          ),
          'operations' => array(
            'data' => t('Operations'),
            'class' => array('rest-ui-operations'),
          ),
        ),
        '#rows' => array(),
      );
      foreach ($available_resources[$status] as $id => $resource) {
        if (strpos($id, 'entity:') === FALSE) {
          $path = '<code>/' . $resource['id'] . '/{id}</code>';
        }
        else {
          $path = '<code>/entity/' . $resource['entity_type'] . '/{id}</code>';
        }

        $list[$status]['table']['#rows'][$id] = array(
          'data' => array(
            'name' => $resource['label'],
            'path' => $path,
            'description' => array(),
            'operations' => array(),
          )
        );

        if ($status == 'disabled') {
          $list[$status]['table']['#rows'][$id]['data']['operations']['data'] = array(
            '#type' => 'operations',
            '#links' => array(
              'enable' => array(
                'title' => t('Enable'),
                'href' => '/admin/structure/rest/resource/' . urlencode($id) . '/enable',
                'query' => array('token' => drupal_get_token('enable')),
              ),
            ),
          );
        }
        else {
          $list[$status]['table']['#rows'][$id]['data']['operations']['data'] = array(
            '#type' => 'operations',
            '#links' => array(
              'edit' => array(
                'title' => t('Edit'),
                'href' => '/admin/structure/rest/resource/' . urlencode($id) . '/edit',

              ),
              'disable' => array(
                'title' => t('Disable'),
                'href' => '/admin/structure/rest/resource/' . urlencode($id) . '/disable',
                'query' => array('token' => drupal_get_token('disable')),
              ),
            ),
          );

          $list[$status]['table']['#rows'][$id]['data']['description']['data'] = array(
            '#theme' => 'restui_resource_info',
            '#resource' => $config[$id],
          );
        }
      }
    }

    $list['enabled']['table']['#empty'] = t('There are no enabled resources.');
    $list['disabled']['table']['#empty'] = t('There are no disabled resources.');

    return $list;
  }

  /**
   * Enables or disables a resource.
   *
   * @param string $resource_id
   *   The identifier or the REST resource.
   * @param string $op
   *   The operation to perform, e.g., 'enable' or 'disable'.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse|\Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirects back to the listing page.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   */
  public function operation($resource_id, $op, Request $request) {
    if (!drupal_valid_token($request->query->get('token'), $op)) {
      // Throw an access denied exception if the token is invalid or missing.
      throw new AccessDeniedHttpException();
    }

    // Perform the operation.
    $resources = config('rest.settings')->get('resources') ?: array();
    $plugin_manager = drupal_container()->get('plugin.manager.rest');
    $plugin = $plugin_manager->getInstance(array('id' => $resource_id));
    if (!empty($plugin)) {
      // Either enable/disable a resource with its default settings.
      // Once enabled, the resource can be configured through the UI.
      if ($op == 'enable') {
        $methods = array_fill_keys($plugin->availableMethods(), array());
        $resources[$resource_id] = $methods;
      }
      else {
        unset($resources[$resource_id]);
      }

      $config = config('rest.settings');
      $config->set('resources', $resources);
      $config->save();

      // Rebuild routing cache.
      drupal_container()->get('router.builder')->rebuild();
      drupal_set_message(t('The resource was @opd successfully.', array('@op' => $op)));
    }

    // Redirect back to the page.
    return new RedirectResponse($this->urlGenerator->generate('restui.list', array(), TRUE));
  }

}
