<?php

/**
 * @file
 * Contains \Drupal\restui\Form\RestUIForm.
 */

namespace Drupal\restui\Form;

use Drupal\system\SystemConfigFormBase;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\Context\ContextInterface;
use Drupal\Core\Extension\ModuleHandler;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Drupal\rest\Plugin\Type\ResourcePluginManager;

/**
 * Manage REST resources.
 */
class RestUIForm extends SystemConfigFormBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * The available Authentication Providers.
   *
   * @var array
   */
  protected $authenticationProviders;

  /**
   * The available serialization formats.
   *
   * @var array
   */
  protected $formats = array();

  /**
   * The REST plugin manager.
   *
   * @var \Drupal\rest\Plugin\Type\ResourcePluginManager
   */
  protected $resourcePluginManager= array();


  /**
   * The URL generator to use.
   *
   * @var \Symfony\Component\Routing\Generator\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * Constructs a \Drupal\user\RestForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\Context\ContextInterface $context
   *   The configuration context.
   * @param \Drupal\Core\Extension\ModuleHandler $module_handler
   *   The module handler.
   * @param array $authenticationProviders
   *   The available Authentication Providers.
   * @param array $formats
   *   The available serialization formats.
   * @param \Drupal\rest\Plugin\Type\ResourcePluginManager $resourcePluginManager
   *   The REST plugin manager.
   */
  public function __construct(ConfigFactory $config_factory, ContextInterface $context, ModuleHandler $module_handler, array $authenticationProviders, array $formats, ResourcePluginManager $resourcePluginManager, UrlGeneratorInterface $url_generator) {
    parent::__construct($config_factory, $context);
    $this->moduleHandler = $module_handler;
    $this->authenticationProviders = $authenticationProviders;
    $this->formats = $formats;
    $this->resourcePluginManager = $resourcePluginManager;
    $this->urlGenerator = $url_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.context.free'),
      $container->get('module_handler'),
      array_keys($container->get('authentication')->getSortedProviders()),
      $container->getParameter('serializer.formats'),
      $container->get('plugin.manager.rest'),
      $container->get('url_generator')
    );
  }

  /**
   * Implements \Drupal\Core\Form\FormInterface::getFormID().
   */
  public function getFormID() {
    return 'restui';
  }

  /**
   * Implements \Drupal\Core\Form\FormInterface::buildForm().
   *
   * @var array $form
   *   The form array.
   * @var array $form_state
   *   The $form_state array.
   * @var string $resource_id
   *   A string that identfies the REST resource.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   */
  public function buildForm(array $form, array &$form_state, $resource_id = NULL) {
    $plugin = $this->resourcePluginManager->getInstance(array('id' => $resource_id));
    if (empty($plugin)) {
      throw new NotFoundHttpException();
    }

    $config = \Drupal::config('rest.settings')->get('resources') ? : array();
    $methods = $plugin->availableMethods();
    $pluginDefinition = $plugin->getPluginDefinition();
    $form['#tree'] = TRUE;
    $form['resource_id'] = array('#type' => 'value', '#value' => $resource_id);
    $form['title'] = array(
      '#markup' => '<h2>' . t('Settings for resource @label', array('@label' => $pluginDefinition['label'])) . '</h2>',
    );
    $form['description'] = array(
      '#markup' => '<p>' . t('Here you can restrict which HTTP methods should this resource support.' .
                             ' And within each method, the available serialization formats and ' .
                             'authentication providers.') . '</p>',
    );
    $form['note'] = array(
      '#markup' => '<p>' . t('<b>Note:</b> Leaving all formats unchecked will enable all of them, while leaving all authentication providers unchecked will default to <code>cookie</code>') . '</p>',
    );
    $form['methods'] = array('#type' => 'container');

    foreach ($methods as $method) {
      $group = array();
      $group[$method] = array(
        '#title' => $method,
        '#type' => 'checkbox',
        '#default_value' => isset($config[$resource_id][$method]),
      );
      $group['settings'] = array(
        '#type' => 'container',
        '#attributes' => array('style' => 'padding-left:20px'),
      );

      // Available formats
      $enabled_formats = array();
      if (isset($config[$resource_id][$method]['supported_formats'])) {
        $enabled_formats = $config[$resource_id][$method]['supported_formats'];
      }
      $group['settings']['formats'] = array(
        '#title' => 'Supported formats',
        '#type' => 'checkboxes',
        '#options' => array_combine($this->formats, $this->formats),
        '#multiple' => TRUE,
        '#default_value' => $enabled_formats,
      );

      // Authentication providers.
      $enabled_auth = array();
      if (isset($config[$resource_id][$method]['supported_auth'])) {
        $enabled_auth = $config[$resource_id][$method]['supported_auth'];
      }
      $group['settings']['auth'] = array(
        '#title' => 'Authentication providers',
        '#type' => 'checkboxes',
        '#options' => array_combine($this->authenticationProviders, $this->authenticationProviders),
        '#multiple' => TRUE,
        '#default_value' => $enabled_auth,
      );
      $form['methods'][$method] = $group;
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * Implements \Drupal\Core\Form\FormInterface::submitForm().
   */
  public function submitForm(array &$form, array &$form_state) {
    $methods = $form_state['values']['methods'];
    $resource_id = $form_state['values']['resource_id'];
    $resources = config('rest.settings')->get('resources') ?: array();
    // Reset the resource configuration.
    $resources[$resource_id] = array();
    foreach ($methods as $method => $settings) {
      if ($settings[$method] == TRUE) {
        $resources[$resource_id][$method] = array();
        // Check for selected formats.
        $formats = array_keys(array_filter($settings['settings']['formats']));
        if (!empty($formats)) {
          $resources[$resource_id][$method]['supported_formats'] = $formats;
        }
        // Check for selected authentication providers.
        $auth = array_keys(array_filter($settings['settings']['auth']));
        if (!empty($auth)) {
          $resources[$resource_id][$method]['supported_auth'] = $auth;
        }
      }
    }

    $config = config('rest.settings');
    $config->set('resources', $resources);
    $config->save();

    // Rebuild routing cache.
    drupal_container()->get('router.builder')->rebuild();
    drupal_set_message(t('The resource was updated successfully.'));
    // Redirect back to the listing.
    $form_state['redirect'] = $this->urlGenerator->generate('restui.list', array(), TRUE);
  }

}
